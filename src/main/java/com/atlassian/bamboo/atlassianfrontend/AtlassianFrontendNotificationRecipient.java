package com.atlassian.bamboo.atlassianfrontend;

import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AtlassianFrontendNotificationRecipient extends AbstractNotificationRecipient implements NotificationRecipient.RequiresResultSummary
{
    private static String WEBHOOK_URL = "afWebhookUrl";
    private static String PRODUCT = "afProduct";

    private String afWebhookUrl = null;
    private String afProduct = null;

    private TemplateRenderer templateRenderer;

    private ResultsSummary resultsSummary;
    private CustomVariableContext customVariableContext;

    @Override
    public void populate(@NotNull Map<String, String[]> params)
    {
        for (String next : params.keySet())
        {
            System.out.println("next = " + next);
        }
        if (params.containsKey(WEBHOOK_URL))
        {
            int i = params.get(WEBHOOK_URL).length - 1;
            this.afWebhookUrl = params.get(WEBHOOK_URL)[i];
        }
        if (params.containsKey(PRODUCT))
        {
            int i = params.get(PRODUCT).length - 1;
            this.afProduct = params.get(PRODUCT)[i];
        }
    }

    @Override
    public void init(@Nullable String configurationData)
    {

        if (StringUtils.isNotBlank(configurationData))
        {
            String delimiter = "\\|";

            String[] configValues = configurationData.split(delimiter);

            if (configValues.length > 0) {
                afWebhookUrl = configValues[0];
            }
            if (configValues.length > 1) {
                afProduct = configValues[1];
            }
        }
    }

    @NotNull
    @Override
    public String getRecipientConfig()
    {
        StringBuilder recipientConfig = new StringBuilder();
        String delimiter = "|";

        if (StringUtils.isNotBlank(afWebhookUrl)) {
            recipientConfig.append(afWebhookUrl);
        }

        recipientConfig.append(delimiter);

        if (StringUtils.isNotBlank(afProduct)) {
            recipientConfig.append(afProduct);
        }

        recipientConfig.append(delimiter);


        return recipientConfig.toString();
    }

    @NotNull
    @Override
    public String getEditHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getEditTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    private Map<String, Object> populateContext()
    {
        Map<String, Object> context = new HashMap<>();

        if (afWebhookUrl != null)
        {
            context.put(WEBHOOK_URL, afWebhookUrl);
        }

        if (afProduct != null)
        {
            context.put(PRODUCT, afProduct);
        }


        System.out.println("populateContext = " + context.toString());

        return context;
    }

    @NotNull
    @Override
    public String getViewHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getViewTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }



    @NotNull
    public List<NotificationTransport> getTransports()
    {
        return Collections.singletonList(new AtlassianFrontendNotificationTransport(afWebhookUrl, afProduct, resultsSummary, customVariableContext));
    }

    public void setResultsSummary(@Nullable final ResultsSummary resultsSummary)
    {
        this.resultsSummary = resultsSummary;
    }

    //-----------------------------------Dependencies
    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    public void setCustomVariableContext(CustomVariableContext customVariableContext) { this.customVariableContext = customVariableContext; }
}
